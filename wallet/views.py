from django.shortcuts import render
from wallet.forms import TransactionForm
from wallet.models import Wallet, AppTransaction
from django.contrib.auth.decorators import login_required
from django.db.models import F, Q
from django.db import transaction


@login_required(login_url='login/')
def transaction_view(request):
    if request.method == "POST":
        form = TransactionForm(request.POST)
        if form.is_valid():
            print(request.user)
            try:
                with transaction.atomic():
                    from_user = Wallet.objects.get(user=request.user)
                    to_user_wallet = Wallet.objects.get(wallet_id=form.cleaned_data['wallet_id'])

                    from_user.balance = F('balance') - form.cleaned_data['amount']
                    to_user_wallet.balance = F('balance') + form.cleaned_data['amount']
                    from_user.save(update_fields=['balance'])
                    to_user_wallet.save(update_fields=['balance'])
                    AppTransaction.objects.create(
                        from_user=request.user,
                        to_user=to_user_wallet.user,
                        amount=form.cleaned_data['amount'])
            except Exception as e:
                print(e)

    return render(request, 'wallet/transaction.html')


@login_required(login_url='login/')
def transaction_list(request):
    payment_type_method = request.GET.get('type', None)
    if payment_type_method:
        if payment_type_method == 'in':
            wallet = AppTransaction.objects.filter(to_user=request.user)
        elif payment_type_method == 'out':
            wallet = AppTransaction.objects.filter(from_user=request.user)
        else:
            wallet = AppTransaction.objects.filter(create_at=request.user)
    else:
        wallet = AppTransaction.objects.filter(Q(from_user=request.user) | Q(to_user=request.user))
    context = {
        'wallet': wallet,
    }
    return render(request, 'wallet/transaction_list.html', context )